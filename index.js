// Require the framework and instantiate it
const fastify = require('fastify')({
    logger: false
})
// Require FS and Path to read the HTML file
const { readFileSync } = require('fs'),
    { join } = require('path');
  
const htmldata = readFileSync(join(__dirname, './static/index.html'));

// Declare a route
fastify.get('/', function (request, reply) {
    reply
        .code(200)
        .type('text/html')
        .send(htmldata);
})

fastify.get('/index.html', function (request, reply) {
    reply
        .code(200)
        .type('text/html')
        .send(htmldata);
})

// Serve the static files!
fastify.register(require('fastify-static'), {
    root: join(__dirname, './static')
})

// Run the server!
fastify.listen(3000, function (err, address) {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }
    console.log(`server listening on ${address}`)
})