let origBoard;
const huPlayer = 'O',
    aiPlayer = 'X';
const winCombos = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
];
let won = false;


const cells = document.querySelectorAll('.cell');

const startGame = function () {
    document.querySelectorAll('.endgame')[0].style.display = 'none';
    origBoard = Array.from(Array(9).keys());
    cells.forEach((cell) => {
        cell.innerText = '';
        cell.style.removeProperty('background-color');
        cell.style.cursor = 'pointer';
        cell.addEventListener('click', turnClick, false);
        document.querySelector('table').removeEventListener('click', startGame, false);
    });
    won = false;
}

const turnClick = function (square) {
    if (typeof origBoard[square.target.id] == 'number') {
        turn(square.target.id, huPlayer);
        console.log(won, checkTie());
        if (!checkTie() && !won) turn(bestSpot(), aiPlayer);
    }
};

const turn = function (squareID, player) {
    origBoard[squareID] = player;
    cells[squareID].innerText = player;
    cells[squareID].style.cursor = 'not-allowed';
    let gameWon = checkWin(origBoard, player);
    if (gameWon) {
        gameOver(gameWon);
        won = true;
    };
}

const checkWin = function (board, player) {
    let plays = board.reduce((a, e, i) => 
		(e === player) ? a.concat(i) : a, []);
    let gameWon = null;
    for (let [index, win] of winCombos.entries()) {
		if (win.every(elem => plays.indexOf(elem) > -1)) {
			gameWon = {index: index, player: player};
			break;
		}
	}
    return gameWon;
}

const gameOver = function (gameWon) {
    for (let index of winCombos[gameWon.index]) {
        document.getElementById(index).style.backgroundColor = (gameWon.player == huPlayer ? 'blue' : 'red');
    };
    cells.forEach((cell) => {
        cell.removeEventListener('click', turnClick);
    })
    declareWinner(gameWon.player === huPlayer ? 'You Win!' : 'You lose :/' );
}

const bestSpot = function () {
    return minimax(origBoard, aiPlayer).index; 
}

const emptySquares = function () {
    return origBoard.filter(s => typeof s == 'number');
}

const checkTie = function () {
    if (emptySquares().length == 0 && !won) {
        cells.forEach((cell) => {
            cell.style.backgroundColor = 'green';
            cell.removeEventListener('click', turnClick, false);
            won = true;
        });
        declareWinner('Tie Game');
        return true;
    }
    return false;
}

const declareWinner = function (who) {
    document.querySelector('.endgame').style.display = 'block';
    document.querySelector('.endgame .text').innerText = who;
}

function minimax (newBoard, player) {
    let availSpots = emptySquares(newBoard); 

    if (checkWin(newBoard, player)) {
        return {score: -10}
    } else if (checkWin(newBoard, aiPlayer)) {
        return {score: 20}
    } else if (availSpots.length === 0) {
        return {score: 0}
    };

    let moves = [];

    for (let i = 0; i < availSpots.length; i++) {
        let move = {};
        move.index = newBoard[availSpots[i]];
        newBoard[availSpots[i]] = player;

        if (player == aiPlayer) {
            let result = minimax(newBoard, huPlayer);
            move.score = result.score;
        } else {
            let result = minimax(newBoard, aiPlayer);
            move.score = result.score;
        };

        newBoard[availSpots[i]] = move.index;

        moves.push(move);
    }

    let bestMove;

    if (player === aiPlayer) {
        let bestScore = -10000;
        for (let i = 0; i < moves.length; i++) {
            if (moves[i].score > bestScore) {
                bestScore = moves[i].score;
                bestMove = i;
            }
        }
    } else if (player === huPlayer) {
        let bestScore = 10000;
        for (let i = 0; i < moves.length; i++) {
            if (moves[i].score < bestScore) {
                bestScore = moves[i].score;
                bestMove = i;
            }
        }
    }

    return moves[bestMove];
}

startGame();

document.querySelector('button.replay').addEventListener('click', startGame);

navigator.serviceWorker?.register('/sw.js', {
    scope: '/'
});