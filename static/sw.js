const cacheName = 'pwa-cache';
const filesToCache = [
  './',
  './index.html',
  './css/style.css',
  './js/script.js',
  './fonts/SketchMatch.ttf'
];

function PurgeCache() {
  caches.delete(cacheName);
}

function WriteCache(e) {
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    }).catch(console.error)
  );
}

/* Start the service worker and cache all of the app's content */
self.addEventListener('install', function(e) {
  // PurgeCache();
  if (navigator.onLine) PurgeCache();
  WriteCache(e);
  setInterval(function () {
    if (navigator.onLine) {
      PurgeCache();
      WriteCache();
    }
  }, 60000)
});

/* Serve cached content when offline */
self.addEventListener('fetch', function(e) {
  e.respondWith(
    caches.match(e.request).then(function(response) {
      return response || fetch(e.request);
    })
  );
});